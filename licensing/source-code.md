# Licensing source code

## Close source

The source code is not licensed.

Copyright notice header:

```text
Copyright <<author>> - All Rights Reserved

This file is part of <<project_name>>.

<<project_name>> is proprietary and confidential: you are not allowed to use, copy, modify or distribute this file, its content or any part of it, via any medium.
```

## Free and Open source

Use the [GNU Affero General Public License](./agpl.txt).

### Header:

```text
Copyright <<date_span>> <<author>>

This file is part of <<project_name>>.

<<project_name>> is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

<<project_name>> is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with <<project_name>>.  If not, see <http://www.gnu.org/licenses/>.
```

### License information

![AGPLv3][agpl]

The source code is licensed under the terms of the **GNU Affero General Public License v3.0**.

See [agpl.txt](agpl.txt) for details.

[agpl]: ./agplv3-medium.png

```markdown
![AGPLv3][agpl]

The source code is licensed under the terms of the **GNU Affero General Public License v3.0**.

See [agpl.txt](agpl.txt) for details.

[agpl]: ./agplv3-medium.png
```