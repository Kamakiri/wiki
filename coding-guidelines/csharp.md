# C# Coding Guidelines

> 💡 [Guidelines are not rules](https://www.youtube.com/watch?v=WJVBvvS57j0)

Version **1.0.0**, last updated on 10/12/2018.

<a href="./csharp.editorconfig" download=".editorconfig">Download .editorconfig</a> (covers only part of the guidelines)

Summary:

- [Formatting](#formatting)
- [Structure](#structure)
- [Naming conventions](#naming-conventions)
- [Readability](#readability)
- [Comments](#comments)
  - [XML Documentation](#xml-documentation)
  - [Other comments](#other-comments)
- [Best practices](#best-practices)

## Formatting

- Encoding **MUST** be UTF-8 without BOM.
- Indent **MUST** be 4 [spaces](https://unicode-table.com/en/0020/) characters.
- Line endings **SHOULD** be CRLF. Line endings **MUST** be consistent across a single code base.
- Code **SHOULD** follow Allman indentation style

  ```csharp
  // SHOULD
  public class Foo
  {
      private void Bar()
      {
          if (baz)
          {
              // ...
          }
          else
          {
              // ...
          }
      }
  }

  // SHOULD NOT
  public class Foo {
      private void Bar() {
          if (baz) {
              // ...
          } else {
              // ...
          }
      }
  }
  ```

- Code **SHOULD NOT** contain 2 consecutive blank lines.
- Code **SHOULD NOT** contain 2 consecutive whitespaces, except for indentation.
- Code **SHOULD NOT** contain any trailing whitespaces.

[↑ TOP](#c-coding-guidelines)

## Structure

- Fields **MUST** be either `private` or `protected`.
- `using` directives **SHOULD** be specified outside of `namespace` declarations.
- `using` directives **SHOULD** be sorted alphabetically, with the exception of `System` directives that **SHOULD** go first.

- The visibility modifiers **SHOULD** always be specified, even if they correspond to the default value.

- Class members **SHOULD** be separated by blank lines. Blank lines between fields **MAY** be omitted.

- Class members **SHOULD** be ordered by groups according to the following order:
  - Constant Fields
  - Fields
  - Constructors
  - Finalizers
  - Delegates
  - Events
  - Enums
  - Interface implementations
  - Properties
  - Indexers
  - Methods
  - Structs
  - Classes

- Class members inside of each group **SHOULD** be ordered by access according to the following order:
  - public
  - internal
  - protected internal
  - protected
  - private

- Class members inside of each group, ordered by access, **SHOULD** be orered by static then non-static.

- Class members inside of each group, ordered by access and static, **SHOULD** be ordered by readonly then non-readonly.

[↑ TOP](#c-coding-guidelines)

## Naming conventions

- Constants **SHOULD** be in `UPPER_SNAKE_CASE`.
- Fields **SHOULD** be in `_camelCase` prefixed with an underscore.
- Variables and parameters **SHOULD** be in `camelCase`.
- Names **SHOULD NOT** contain hungarian notation.
- Other members **SHOULD** be in `PascalCase`.
- Namespaces **SHOULD** be in `PascalCase.PascalCase`.
- Interface names **SHOULD** be prefixed with `I`. Other types **SHOULD NOT** be prefixed.
- Events **SHOULD** be prefixed with `On` (e.g. `OnClose`).
- Acronyms **SHOULD** be treated as words (e.g. `siteUrl`).
- Enums **SHOULD** be named with with singular nouns, except for bit fields that **SHOULD** be named with plural nouns.

  ```csharp
  // SHOULD
  public enum Color
  {
      Blue,
      White,
      Red
  }

  //SHOULD
  [Flags]
  public enum Colors
  {
      Blue = 1 << 0,
      White = 1 << 1,
      Red = 1 << 2
  }
  ```

[↑ TOP](#c-coding-guidelines)

## Readability

- All names **MUST** be in English.
- `this` prefix **SHOULD** be avoided.
- Lines longer than 200 characters **SHOULD** be avoided.
- `var` keyword **SHOULD** only be used when the type is obvious.
- Each type **SHOULD** be defined in a separated file. This file name **SHOULD** match the type name (without prefix).
- A single line **SHOULD NOT** contain multiple statements.
- Splitted method chain **SHOULD** place a single call per line.

  ```csharp
  // SHOULD
  string[] sharedBooktitles = users
      .SelectMany(user => user.Books)
      .Where(book => book.IsShared)
      .Select(book => book.Title)
      .ToArray();

  // SHOULD NOT
  string[] sharedBooktitles = users
      .SelectMany(user => user.Books)
      .Where(book => book.IsShared)
      .Select(book => book.Title).ToArray();
  ```

- Language keywords **SHOULD** be used instead of BCL types (e.g. `int` instead of `Int32`).
- `#region` directives **SHOULD NOT** be used.
- Unsigned types **SHOULD** be avoided.
- Object and collection initializers **SHOULD** be prefered to assignments.

  ```csharp
  // SHOULD
  var player = new Player("Alice")
  {
      Age = 45,
      Score = 13568
  };

  // SHOULD NOT
  var player = new Player("Alice");
  player.Age = 45;
  player.Score = 13568;
  ```

- Early return statements **SHOULD** be used to avoid unnecessary nesting.

  ```csharp
  // SHOULD
  public void Foo(int bar)
  {
      if (bar < 42) return;

      // Do something.
  }

  // SHOULD NOT
  public void Foo(int bar)
  {
      if (bar >= 42)
      {
          // Do something.
      }
  }
  ```

- Tuple names **SHOULD** be prefered to ItemX properties.
- Uncommon abbreviations **SHOULD** be avoided.
- Auto-properties **SHOULD** be prefered to properties with backing field.
- Name redundancy **SHOULD** be avoided.

  ```csharp
  // SHOULD
  public class User
  {
      public string Name { get; set; }
  }

  // SHOULD NOT
  public class User
  {
      public string UserName { get; set; }
  }
  ```

[↑ TOP](#c-coding-guidelines)

## Comments

- All comments **MUST** be in English.

### XML Documentation

- All public APIs designed for redistribution **MUST** be documented.
- Comments in the description tags **SHOULD** be full sentences, beginning with a capital letter and ending with a period.

### Other comments

- All comments **MUST** be necessary.
- Comments **SHOULD** be full sentences, beginning with a capital letter and ending with a period.
- A single whitespace **MUST** be inserted after the comment delimiter.
- All comment decoration **SHOULD** be avoided.

[↑ TOP](#c-coding-guidelines)

## Best practices

- UTC time **SHOULD** always be used.
- Conditional statements **MUST** either be written on a single line or enclosed with braces.

  ```csharp
  // MUST
  if (foo) Bar();
  if (foo)
  {
      Bar();
  }

  // MUST NOT
  if (foo)
      Bar();
  ```

- `nameof` expressions **MUST** be used instead of literal names.
- Conditional operators (`&&` and `||`) **SHOULD** be used instead of simple ones (`&` and  `|`).
- Bit fields **MUST** be initialized explicitly. Initialization **SHOULD** use shifting notation.

  ```csharp
  // MUST, SHOULD
  [Flags]
  public enum Colors
  {
      Blue = 1 << 0,
      White = 1 << 1,
      Red = 1 << 2
  }

  // SHOULD NOT
  [Flags]
  public enum Colors
  {
      Blue = 1,
      White = 2,
      Red = 4
  }

  // MUST NOT
  [Flags]
  public enum Colors
  {
      Blue,
      White,
      Red
  }
  ```

- Non bit fields enums **SHOULD** not specify values nor types.
- Hiding inherited members with the `new` keyword **SHOULD** be avoided.
- `struct` **SHOULD** be used only if all the following conditions are true:
  - It represents a single value
  - An instance weights under 16 bytes
  - It's immutable
  - It won't need frequent boxing
- Abstract classes **SHOULD** only define `protected` or `internal` constructors.
- Marker (empty) interfaces **SHOULD** be avoided.

[↑ TOP](#c-coding-guidelines)