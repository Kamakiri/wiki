# TypeScript Coding Guidelines

> 💡 [Guidelines are not rules](https://www.youtube.com/watch?v=WJVBvvS57j0)

Version **1.0.0**, last updated on 10/12/2018.

Summary:

- [Formatting](#formatting)
- [Structure](#structure)
- [Naming conventions](#naming-conventions)
- [Readability](#readability)
- [Comments](#comments)
  - [JSDoc](#jsdoc)
  - [Other comments](#other-comments)
- [Best practices](#best-practices)

## Formatting

- Encoding **MUST** be UTF-8 without BOM.
- Indent **MUST** be 4 [spaces](https://unicode-table.com/en/0020/) characters.
- Line endings **SHOULD** be CRLF. Line endings **MUST** be consistent across a single code base.
- Code **SHOULD** follow 1TBS indentation style

  ```ts
  // SHOULD
  class MyClass {
      function doSomething() {
          if (condition) {
              // ...
          } else {
              // ...
          }
      }
  }

  // SHOULD NOT
  class MyClass
  {
      function doSomething()
      {
          if (condition)
          {
              // ...
          }
          else
          {
              // ...
          }
      }
  }
  ```

- Code **SHOULD NOT** contain 2 consecutive blank lines.
- Code **SHOULD NOT** contain 2 consecutive whitespaces, except for indentation.
- Code **SHOULD NOT** contain any trailing whitespaces.

[↑ TOP](#typescript-coding-guidelines)

## Structure

- Imports **SHOULD** be standard and minimal.

  ```ts
  // SHOULD
  import { foo, bar } from 'baz'

  // SHOULD NOT
  import Baz from 'baz';

  // SHOULD NOT
  const Baz = require('baz');
  ```

- Imports from the same module **SHOULD** be grouped in a single statement.

  ```ts
  // SHOULD
  import { foo, bar } from 'baz';

  // SHOULD NOT
  import { foo } from 'baz';
  import { bar } from 'baz';
  ```

- Imports **SHOULD** be placed at the top of the file, ordered alphabetically.
- Classes **SHOULD** be prefered to prototypes and constructor functions.

[↑ TOP](#typescript-coding-guidelines)

## Naming conventions

- Constants **MAY** be in `UPPER_SNAKE_CASE`.
- Variables, parameters and class members **SHOULD** be in `camelCase`.
- Names **SHOULD NOT** contain hungarian notation.
- Classes, interfaces, enums and enum members **SHOULD** be in `PascalCase`.
- Interface names **SHOULD** be prefixed with `I`. Other types **SHOULD NOT** be prefixed.
- Acronyms **SHOULD** be treated as words (e.g. `siteUrl`).
- Enums **SHOULD** be named with with singular nouns, except for bit fields that **SHOULD** be named with plural nouns.

  ```ts
  // SHOULD
  enum Color {
      Blue,
      White,
      Red
  }

  //SHOULD
  enum Colors {
      None = 0,
      Blue = 1 << 0,
      White = 1 << 1,
      Red = 1 << 2
  }
  ```

[↑ TOP](#typescript-coding-guidelines)

## Readability

- All names **MUST** be in English.
- Prefer `Foo[]` to `Array<Foo>` type annotation.
- Lines longer than 200 characters **SHOULD** be avoided.
- A single line **SHOULD NOT** contain multiple statements.
- Use `number`, `boolean`, `string` and `object` rather than `Number`, `Boolean`, `String` and `Object` types
- Splitted method chain **SHOULD** place a single call per line.

  ```ts
  // SHOULD
  const foo = bar
      .filter(b => b.baz > 42)
      .map(b => b.qux)
      .reduce((a, b) => a.concat(b));

  // SHOULD NOT
  const foo = bar
      .filter(b => b.baz > 42)
      .map(b => b.qux).reduce((a, b) => a.concat(b));
  ```

- Early return statements **SHOULD** be used to avoid unnecessary nesting.

  ```ts
  // SHOULD
  function foo(age) {
      if (age < 18) return;

      // Do something.
  }


  // SHOULD NOT
  function foo(age) {
      if (age >= 18) {
          // Do something.
      }
  }
  ```

- Litteral initialization **SHOULD** be prefered.

  ```ts
  // SHOULD
  const foo = {};
  const bar = [];

  // SHOULD NOT
  const foo = new Object();
  const bar = new Array();
  ```

- Name redundancy **SHOULD** be avoided.

  ```ts
  // SHOULD
  class User {
      name: string;
  }

  // SHOULD NOT
  public class User {
      userName: string;
  }
  ```

[↑ TOP](#typescript-coding-guidelines)

## Comments

- All comments **MUST** be in English.

### JSDoc

- All public APIs designed for redistribution **MUST** be documented.
- Comments in the description tags **SHOULD** be full sentences, beginning with a capital letter and ending with a period.

### Other comments

- All comments **MUST** be necessary.
- Comments **SHOULD** be full sentences, beginning with a capital letter and ending with a period.
- A single whitespace **MUST** be inserted after the comment delimiter.
- All comment decoration **SHOULD** be avoided.

[↑ TOP](#typescript-coding-guidelines)

## Best practices

- Interfaces **MUST NOT** be written in `.d.ts` files (see <https://stackoverflow.com/a/42257742>).
- Namespaces **SHOULD** be used to organize complex code
- Conditional statements **MUST** either be written on a single line or enclosed with braces.

  ```ts
  // MUST
  if (foo) bar();
  if (foo) {
      bar();
  }

  // MUST NOT
  if (foo)
      bar();
  ```

- Implied global variables **SHOULD NOT** be used.
- `eval` function **SHOULD NOT** be used.
- `Function` constructor **SHOULD NOT** be used.
- Identity operators (`===` and `!==`) **SHOULD** be used instead of equality operators (`==` and `!=`).
- Default parameter **SHOULD** be used instead of argument mutation.

  ```ts
  // SHOULD
  function foo(bar = {}) {
      // ...
  }

  // SHOULD NOT
  function foo(bar) {
      bar = bar || {};
  }
  ```

- `var` **MUST NOT** be used.
- `const` **SHOULD** be used whenever possible.
- Single quotes **SHOULD** be prefered to double quotes.
- Interpolation **SHOULD** bu used rather than concatenation.

  ```ts
  // Do
  const foo = `Hello ${name}`;

  // Don't
  const foo = 'Hello ' + name;
  ```

- All statements **MUST** end with a semicolon.
- `void` **MUST** be used rather than `any` for return type of callbacks if the return value is ignored
- More specific overloads **MUST** be written first.

  ```ts
  // SHOULD
  function foo(bar: string): number { }
  function foo(bar: any): object { }

  // SHOULD NOT
  function foo(bar: any): number { }
  function foo(bar: string): object { }
  ```

- Prefer optional parameters to overloads.
- Prefer unions to overloads.

[↑ TOP](#typescript-coding-guidelines)