# Git Workflow

## Only before version 1.0.0

- `master` branch can contain any code

## Only after version 1.0.0

- `master` branch contain only release code

## Always

- `staging` branch is used to collect working branches before merging to master
- Name branches using `snake_case`
- Prefix branch names with issue number (e.g. `42_foo_bar`)
- Start commit description with `#<issue number>`