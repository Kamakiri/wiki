# Versioning

- Use [Semantic Versioning 2.0.0](https://semver.org/spec/v2.0.0.html) (initialize at `0.1.0`)
- Create a git tag for each version