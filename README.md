# Kamakiri Wiki

>[![Creative Commons License](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)](http://creativecommons.org/licenses/by-sa/4.0/)
> This wiki, by [Nicolas Maurice](https://gitlab.com/Kyrdairrin), is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).

The key words “MUST”, “MUST NOT”, “REQUIRED”, “SHALL”, “SHALL NOT”, “SHOULD”, “SHOULD NOT”, “RECOMMENDED”, “MAY”, and “OPTIONAL” are to be interpreted as described in [RFC 2119](https://tools.ietf.org/html/rfc2119).

## Conventions

- [Units](./conventions/units.md)

## Coding Guidelines

- [C#](./coding-guidelines/csharp.md)
- [TypeScript](./coding-guidelines/typescript.md)

## Licensing

- [Licensing source code](./licensing/source-code.md)
- [Licensing documentation](./licensing/documentation.md)
- [Licensing creative content](./licensing/creative-content.md)

## Process

- [Git workflow](./process/git-workflow.md)
- [Versioning](./process/versioning.md)
