# Units Conventions

## General

Use the [International System of Units](https://en.wikipedia.org/wiki/International_System_of_Units) (aka *modern metric system*).

## Date

All dates **SHOULD** follow one of these two formats:

- yyyy-mm-dd
- dd/mm/yyyy

## Time

Time **SHOULD** be expressed with the 24 hours notation.

Reference time **SHOULD** be UTC.
